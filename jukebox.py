import vlc
import codecs
import re
import datetime
from xml.etree import ElementTree
import sys
import subprocess
import time
import os

import logging

log = logging.getLogger( __name__ )
logging.basicConfig( stream=sys.stderr, level=logging.DEBUG )

class _Getch:
    """Gets a single character from standard input.  Does not echo to the
screen."""
    def __init__(self):
        try:
            self.impl = _GetchWindows()
        except ImportError:
            self.impl = _GetchUnix()

    def __call__(self): return self.impl()


class _GetchUnix:
    def __init__(self):
        import tty, sys

    def __call__(self):
        import sys, tty, termios
        fd = sys.stdin.fileno()
        old_settings = termios.tcgetattr(fd)
        try:
            tty.setraw(sys.stdin.fileno())
            ch = sys.stdin.read(1)
        finally:
            termios.tcsetattr(fd, termios.TCSADRAIN, old_settings)
        return ch

class _GetchWindows:
    def __init__(self):
        import msvcrt

    def __call__(self):
        import msvcrt
        return msvcrt.getch()

getch = _Getch()

class Player ( object ):
    def __init__ ( self ):
        self.inst = vlc.libvlc_new( 0, None )
        self.media_player = None

    def play_url ( self, url, block = False ):
        self.stop()

        m = vlc.libvlc_media_new_location( self.inst, url )
        self.media_player = vlc.libvlc_media_player_new_from_media( m )
        vlc.libvlc_media_release( m )

        vlc.libvlc_media_player_play( self.media_player )

        if block:
            state = None
            while state != 'State.Ended':
                time.sleep(0.1)
                state = str(vlc.libvlc_media_player_get_state( self.media_player ))

    def play ( self, url = None, title = None ):
        if title is not None:
            title_file = 'title.wav'
            successful = render_message_to_wave( title, title_file )
            if successful:
                self.play_url( 'file://{0}'.format(os.path.abspath(title_file)), block = True )

        if url is not None:
            return self.play_url( url )

        if self.media_player is not None:
            vlc.libvlc_media_player_play( self.media_player )

    def pause ( self, url = None ):
        if self.media_player is not None:
            vlc.libvlc_media_player_pause( self.media_player )

    def stop ( self ):
        if self.media_player is not None:
            vlc.libvlc_media_player_stop( self.media_player )
            vlc.libvlc_media_player_release( self.media_player )
            self.media_player = None

    def release ( self ):
        self.stop()
        vlc.libvlc_release( self.inst )

def read_pls ( filename ):
    d = dict()
    entry_default = dict.fromkeys( ['title', 'url' ], None )
    re_file = re.compile( '[Ff]{1}ile([0-9])=(.+)' )
    re_title = re.compile( '[Tt]{1}itle([0-9])=(.+)' )
    with codecs.open( filename, 'r', encoding='utf-8' ) as f:
        lines = f.read().splitlines()
    for line in lines:
        m = re.match( re_file, line )
        if m:
            i, url = m.groups()
            i = int( i )
            entry = d.get( i, entry_default )
            entry['url'] = url
            d[i] = entry
            continue
        m = re.match( re_title, line )
        if m:
            i, title = m.groups()
            i = int( i )
            entry = d.get( i, entry_default )
            entry['title'] = title
            d[i] = entry
            continue
    return d

def read_xspf ( filename ):
    et = ElementTree.parse( filename )
    def tagname ( name ):
        return '{{http://xspf.org/ns/0/}}{0}'.format( name )

    def first_child ( node, name ):
        children = node.findall( tagname( name ) )
        if children is not None and len(children) > 0:
            return children[0]
        else:
            return None

    root = et.getroot()
    tracklist = root.findall( tagname( 'trackList' ) )[0]
    playlist = []
    for i, track in enumerate( tracklist ):
        fc = first_child( track, 'title' )
        if fc is not None:
            title = fc.text
        else:
            title = 'title {0}'.format( i )

        location = first_child( track, 'location' ).text
        playlist.append( { 'title' : title, 'location': location } )
    return playlist

class Digits ( object ):
    def __init__ ( self, n = 2 ):
        self.n = n
        self.timeout = datetime.timedelta( seconds = 1 )
        self.datetime_last_key = None
        self.reset()
        #self.setup()

#    def setup ( self ):
#        self.stdscr = curses.initscr()
#        curses.cbreak()
#        curses.noecho()
#        self.stdscr.keypad(1)

    def reset ( self ):
        self.digits = [ 0 ] * self.n
        self.i = 0

    def read ( self ):
        key = getch()
        if ord( key ) == 3:
            sys.exit(1)
        if key not in '0123456789':
            return None
        digit = int( key )

        now = datetime.datetime.now()

        need_reset = True
        if self.datetime_last_key is None:
            need_reset = False
        else:
            td = now - self.datetime_last_key
            if td < self.timeout:
                need_reset = False
        if need_reset:
            self.reset()
        self.datetime_last_key = now

        if self.i < self.n:
            self.digits[self.i] = digit

        if self.i >= ( self.n - 1 ):
            digits_return = self.digits[:]
            self.reset()
            return digits_return
        else:
            self.i = self.i + 1
            return None

def number ( digits ):
    ps = list( reversed( [ 10**i for i in range( len(digits) ) ] ) )
    n = 0
    for i, p in zip( digits, ps ):
        n += i*p
    return n

def render_message_to_wave ( msg, filename = 'title.wav' ):
    r = subprocess.call( ['espeak', '-vfemale3', '-w{0}'.format(filename), msg] )
    return r == 0

class JukeBox ( object ):
    def __init__ ( self, filename ):
        self.player = Player()
        self.filename = filename
        self.playlist = []
        #import pdb; pdb.set_trace()
        self.digits = Digits()
        self.reload_playlist()

    def reload_playlist( self ):
        self.playlist = read_xspf( self.filename )

    def run ( self ):
        log.debug('Waiting for numbers..')
        while True:
            d = self.digits.read()
            if d:
                n = number( d )
                log.debug( 'number: {0}'.format( n ) )
                if n == 0:
                    log.debug( 'Stop.' )
                    self.player.stop()
                if n >= 1 and n <= len( self.playlist ):
                    track = self.playlist[ n - 1 ]
                    log.debug( 'Play {0}.'.format( track['title'] ) )
                    self.player.play( track['location'], track['title'] )

if __name__ == '__main__':
    log.debug('starting')
    jb = JukeBox( 'stations.xspf' )
    jb.run()
